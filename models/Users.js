module.exports = (sequelize, DataTypes) => {
    const Users = sequelize.define('Users', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
            allowNull: false
        },
            name: {
            type: DataTypes.STRING,
            allowNull: false
        },
            password: {
            type: DataTypes.STRING,
            allowNull: false
        },
            createdAt: {
            type: DataTypes.DATE
        },
            updatedAt: {
            type: DataTypes.DATE
        },
            deletedAt: {
            type: DataTypes.DATE
        }
    }, 
    {
        tableName: 'users'
    });
    
    return Users;
}