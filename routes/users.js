var express = require('express');
var router = express.Router();
const Validator = require('fastest-validator');

const { Users } = require('../models');

const v = new Validator();

//create users
router.post('/', async (req, res) => {
  const schema = {
    name: 'string'
  }
  const validate = v.validate(req.body, schema);
  if (validate.length){
    return res.status(400).json(validate);
  }

  const users = await Users.create(req.body);

  return res.status(200).json({
    status: 200,
    message: "Berhasil Simpan User",
    data: users
  });
});


//updated users
router.put('/:id', async (req, res) => {
  const id = req.params.id;
  let users = await Users.findByPk(id);
  
  if (!users){
    return res.status(404).json({
      status: 404,
      message: "User tidak ditemukan"
    });
  }

  const schema = {
    name: 'string|optional'
  }
  const validate = v.validate(req.body, schema);
  if (validate.length){
    return res.status(400).json(validate);
  }

  users = await users.update(req.body);

  return res.status(200).json({
    status: 200,
    message: "Berhasil Update User",
    data: users
  });
});


//read users
router.get('/', async (req, res) => {
  const users = await Users.findAll();
  return res.status(200).json({
    status: 200,
    data: users
  });
});


//detail users
router.get('/:id', async (req, res) => {
  const id = req.params.id;
  const users = await Users.findByPk(id);

  if (users!==null){
    return res.status(200).json({
      status: 200,
      data: users
    });
  } else {
    return res.status(404).json({
      status: 404,
      message: 'User tidak ditemukan'
    });
  }
});


// delete users
router.delete('/:id', async (req, res) => {
  const id = req.params.id;
  const users = await Users.findByPk(id);
  
  if (!users){
    return res.status(404).json({
      status: 404,
      message: "User tidak ditemukan"
    });
  }

  await users.destroy();

  return res.status(200).json({
    status: 200,
    message: "User berhasil dihapus"
  });
})

module.exports = router;
