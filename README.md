# Pintap Test

Backend Test Pintap

# Installation

Jika menggunakan localhost :

1. Buat nama db
2. sesuaikan file .env dengan db 
3. Kemudian lakukan migrate db -> npx sequelize db:migrate
4. npm start

# API Documentation

## Create User

URL : POST https://xsgwtfacs3.execute-api.us-east-1.amazonaws.com/dev/users

Format body bisa menggunakan json atau x-www-formurlencoded.

Parameter yg diperlukan:

| Parameter     | Wajib    | Tipe     | Keterangan       |
| ---           | ---      | ---      | --               |
| name          | Ya       | String   | nama user        |
| password      | Ya       | String   | password user    | 


## Get User (List User)

URL : GET https://xsgwtfacs3.execute-api.us-east-1.amazonaws.com/dev/users

Detail User
URL : GET https://xsgwtfacs3.execute-api.us-east-1.amazonaws.com/dev/users/{id}

Parameter id = ID User


## Update User

URL : PUT https://xsgwtfacs3.execute-api.us-east-1.amazonaws.com/dev/users/{id}

Format body bisa menggunakan json atau x-www-formurlencoded.

Parameter yg diperlukan:

| Parameter     | Wajib       | Tipe     | Keterangan       |
| ---           | ---         | ---      | --               |
| name          | Tidak       | String   | nama user        |
| password      | Tidak       | String   | password user    | 


## Delete User

URL : DELETE https://xsgwtfacs3.execute-api.us-east-1.amazonaws.com/dev/users/{id}

Parameter id = ID User